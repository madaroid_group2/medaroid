﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**********************************************
 * MEDAROID EDITOR WINDOW
 * GUI Controller for creating medaroid data
 * ********************************************/
public class MedaroidEditorWindow : EditorWindow
{
    public static MedaroidEditorWindow instance;

    public static List<Medaroid> mData;
    private Vector2 scroll;

    static MedaroidEditorWindow()
    {
        mData = MedaroidEditor.GetData();
        if (mData == null)
        {
            mData = new List<Medaroid>();
        }
    }

    public static void OpenWindow()
    {
        instance = (MedaroidEditorWindow)EditorWindow.GetWindow(typeof(MedaroidEditorWindow));
        instance.title = "Medaroid Editor";
    }


    void OnGUI()
    {
        EditorGUILayout.BeginVertical("box");
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Insert New"))
        {
            mData.Add(new Medaroid());
        }
        if (GUILayout.Button("Remove All"))
        {
        }
        if (GUILayout.Button("Save"))
        {
            Save();
        }
        if (GUILayout.Button("Backup"))
        {
            Save();
        }
        if (GUILayout.Button("Load Backup"))
        {
            Load();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        GUILayout.Label("Medaroid List:", EditorStyles.boldLabel);
        scroll = GUILayout.BeginScrollView(scroll);
        for (int i = 0; i < mData.Count; i++)
        {
			EditorGUILayout.BeginVertical("box");
			EditorGUILayout.BeginHorizontal();
			mData[i].origin = EditorGUILayout.TextField("Original (元ネタ)" ,mData[i].origin);
			mData[i].name = EditorGUILayout.TextField("Name (名前)", mData[i].name);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			GUILayout.Label("Description (説明文):");
			mData[i].description = EditorGUILayout.TextArea(mData[i].description);
			EditorGUILayout.Separator();
			EditorGUILayout.BeginHorizontal();
			mData[i].size = EditorGUILayout.IntField("Size (大きさ)", mData[i].size);
			mData[i].weight = EditorGUILayout.IntField("Weight (重さ)", mData[i].weight);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			mData[i].speed = EditorGUILayout.IntField("Speed (スピード)", mData[i].speed);
			mData[i].power = EditorGUILayout.IntField("Power (総合力)", mData[i].power);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			mData[i].rarelity = EditorGUILayout.IntField("Rarelity (レアリティ)", mData[i].rarelity);
			mData[i].gachaPercent = EditorGUILayout.FloatField("Gacha Percentage (ガチャ出現率)", mData[i].gachaPercent);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			mData[i].money = EditorGUILayout.IntField("Money (所持マニー)", mData[i].money);
			mData[i].score = EditorGUILayout.IntField("Score (所持スコア)", mData[i].score);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
            mData[i].id = i + 1;
            GUILayout.Label("Medaroid " + mData[i].id, EditorStyles.boldLabel);
            if (GUILayout.Button("Insert Above"))
            {
                mData.Insert(i, new Medaroid());
            }
            if (GUILayout.Button("Insert"))
            {
                mData.Insert(i + 1, new Medaroid());
            }
            if (GUILayout.Button("Remove"))
            {
                mData.Remove(mData[i]);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }
        GUILayout.EndScrollView();
    }

    void Save() {
        MedaroidEditor.Save();
    }
    void Load() {
        mData = MedaroidEditor.GetData();
    }
}
