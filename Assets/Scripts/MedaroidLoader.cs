﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class MedaroidLoader
{
    static string resourcePath = "Data/Medaroid";
    static string spritePath = "Medaroid/";

    static List<Sprite> sprites;
    static Dictionary<string, int> spriteIndex;
    static Dictionary<int, int> idIndex = new Dictionary<int,int>();
    
    // Load all Medaroid data from xml resouces
    public static List<Medaroid> LoadAll()
    {
        List<Medaroid> data = GetData();
        
        for (int i = 0; i < data.Count; i++)
        {
            //data[i].sprite = sprites[GetIndex(data[i].name)];
			Texture2D tex = (Texture2D)Resources.Load(spritePath + data[i].id);
            data[i].sprite = Sprite.Create(tex, new Rect(0,0,tex.width, tex.height), new Vector2(0.5f, 0.5f));
            data[i].sprite.name = data[i].name;
            if (!idIndex.ContainsKey(data[i].id))
            {
                idIndex.Add(data[i].id, i);
            }
            else
            {
                idIndex[data[i].id] = i;
            }
            if (PlayerPrefs.GetString(Strings.DATA_CURRENT_MEDAROID, data[0].name) == data[i].name)
            {
                Attributes.SetMedaroid(data[i]);
            }
        }
        data[0].isPurchased = true;
        foreach (int id in Attributes.GetPurchaseMedaroidsId())
        {
            data[idIndex[id]].isPurchased = true;
        }
        return data;
    }

    static List<Medaroid> GetData()
    {
        List<Medaroid> data = new List<Medaroid>();
        TextAsset file = Resources.Load(resourcePath) as TextAsset;
        XmlSerializer xml = new XmlSerializer(typeof(List<Medaroid>));
        StringReader textData = new StringReader(file.text);
        data = xml.Deserialize(textData) as List<Medaroid>;
        textData.Close();
        return data;
    }

    public static Dictionary<string, int> GetMedaroidNameIndex(List<Medaroid> data)
    {
        Dictionary<string, int> dicData = new Dictionary<string, int>();
        for (int i = 0; i < data.Count; i++)
        {
            dicData.Add(data[i].name, data[i].id);
        }
        return dicData;
    }
}
