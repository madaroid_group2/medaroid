﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;

public class Attributes : MonoBehaviour {

	//the variable will be set to 0 if the PlayerPref does not exist but can be any value
	public static int videoRecord = PlayerPrefs.GetInt(Strings.DATA_VIDEO_RECORD, 0); //1 On, 0 Off
	public static int notification = PlayerPrefs.GetInt(Strings.DATA_NOTIFICATION, 0);//1 On, 0 Off
	public static int bgm = PlayerPrefs.GetInt(Strings.DATA_BGM, 1);//1 On, 0 Off
	public static int se = PlayerPrefs.GetInt(Strings.DATA_SE, 1);//1 On, 0 Off

	public static bool isClickYesGacha=false;

	public static string userName = PlayerPrefs.GetString(Strings.DATA_USER_NAME);
	public static int paidMoneyGacha = 30000; // Tien de gacha
	public static Medaroid currentMedaroid;
	static List<int> purchasedMedaroidsId = new List<int>();

	//Set money = money trong database or default = 1000000
	private static int money = PlayerPrefs.GetInt(Strings.DATA_MONEY, 100000);

	public static void SetUserName(string name){
		userName = name;
		PlayerPrefs.SetString(Strings.DATA_USER_NAME, userName);
	}

	public static void SetNotification(int noti)
	{
		notification = noti;
		PlayerPrefs.SetInt(Strings.DATA_NOTIFICATION, notification);
	}

	//set on, off video recorder
	public static void SetVideoRecorder(int videocor)
	{
		videoRecord = videocor;
		PlayerPrefs.SetInt(Strings.DATA_VIDEO_RECORD, videoRecord);
	}

	//set on,off BGM
	public static void SetBGM(int bg)
	{
		bgm = bg;
		PlayerPrefs.SetInt(Strings.DATA_BGM, bgm);
	}

	//set on, off SE
	public static void SetSE(int s)
	{
		se = s;
		PlayerPrefs.SetInt(Strings.DATA_SE, se);
	}
	//Trả về Money cho màn hình menu
	public static int GetMoney(){
		return money;
	}
	//set money
	public static void SetMoney(int _money){
		money = _money;
	}
	// Set current medaroid
	public static void SetMedaroid(Medaroid med)
	{
		currentMedaroid = med;
		PlayerPrefs.SetString(Strings.DATA_CURRENT_MEDAROID, med.name);
	}
	// Get current medaroid
	public static Medaroid GetMedaroid(){
		return currentMedaroid;
	}

	public static List<int> GetPurchaseMedaroidsId()
	{
		return purchasedMedaroidsId;
	}

}
