﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography.X509Certificates;
/**
 * Author: HaiLeader
 * Date: 7/1/2016
 * 
 * Tất cả các biến đều là CONST
 * Dùng tiền tố ANIM cho animator
 * FX cho effects
 * SE cho sounds
*/
public class Strings {

	public const string ANIM_CLOSELOGIN = "CloseLogin";
	public const string ANIM_CLOSEMENU = "CloseMenu";
	public const string ANIM_CLOSEMENUGACHA = "GachaMenuClose";
	
	public const string TAG_ENEMY = "Medaroid";
	public const string TAG_PLAYER = "Player";
	public const string TAG_MEDROID_KNOCKOUT_POINT = "MedaroidCenterPoint";
	public const string LAYER_MEDAROID = "Medaroid";
	


	public const string SCEN_GAMEPLAY = "GamePlay";
	public const string SCEN_MENU = "Menu";
	public const string SCEN_SETTING = "Setting";
	public const string SCEN_GACHA = "Gacha";
	public const string SCEN_MEDAROID = "Medaroid";
	public const string SCEN_LOGIN = "Login";
	public const string SCEN_SHOP = "Shop";
	public const string SCEN_GACHAEFFECT = "GachaEffect";
	
	
	public const string DATA_MONEY = "secured_data_money";
	public const string DATA_BGM = "bgm";
	public const string DATA_SE = "se";
	public const string DATA_NOTIFICATION = "notification";
	public const string DATA_VIDEO_RECORD = "video_record";
	public const string DATA_CURRENT_MEDAROID = "current_medaroid";
	public const string DATA_USER_NAME = "user_name";
	public const string DATA_CHECK_RECORD = "check_record";

	public const string SPECIAL_CHARACTER =  "@#$%^&*()";

}
