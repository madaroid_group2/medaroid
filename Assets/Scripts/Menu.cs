﻿/**
 * Author: Dao Danh Luu
 * 7/1/2016
 * Xu ly data cho man hinh menu
 * 
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	public Text txtGold;

	// Use this for initialization
	void Start () {
		txtGold.text = Attributes.GetMoney().ToString("n0");
	}

}
