﻿using UnityEngine;
using System.Collections;

public class Medaroid {

	public int id;
	public string origin = "スイカ";       
	public string name = "スイペッペ";     
	public string description = "種をペッペと吐き出し割られる運命に抗う孤独な夏の風物詩"; 
	public int size;           
	public int weight;         
	public int speed;          
	public int power;          
	public int rarelity;       
	public float gachaPercent; 
	public int money;          
	public int score;          
	
	public Sprite sprite;
	
	public bool isPurchased = false;
}
