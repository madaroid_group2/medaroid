﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class LoginController : MonoBehaviour {

	public Animator game; 
	public GameObject popupLoginFailEmptyString; // popup login error empty string
	public GameObject popupLoginFailSpecialCharacter; // popup login error special character
	public Text txtInputLogin;
	public float timer = 1.5f;

	public void LoginMenu(){
		if(CheckEmptyString()){
			popupLoginFailEmptyString.SetActive(true);
		}else if(CheckSpecialCharacter()){
			popupLoginFailSpecialCharacter.SetActive(true);
		}else{
			Attributes.SetUserName(txtInputLogin.text.Trim());
			StartCoroutine("WaitMenu");
		}
	}

	//Close popup
	public void OKLogin(){
		popupLoginFailEmptyString.SetActive(false);
		popupLoginFailSpecialCharacter.SetActive(false);
	}

	// Vao layout menu sau times
	IEnumerator WaitMenu(){
		yield return new WaitForSeconds(timer);
		Application.LoadLevel(Strings.SCEN_MENU);
	}

	// Check loi khong nhap ky tu
	private bool CheckEmptyString(){
		if(txtInputLogin.text.Trim() == "")
			return true;
		return false;
	}
	// Check loi nhap ky tu dac biet
	private bool CheckSpecialCharacter(){
		string username = txtInputLogin.text;
		char[] char_name_array = username.ToCharArray();
		foreach(char c in char_name_array){
			if(Strings.SPECIAL_CHARACTER.IndexOf(c) > 0) return true;
		}
		return false;
	}
}
