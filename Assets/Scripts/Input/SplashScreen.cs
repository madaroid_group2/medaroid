﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {

	public float timer = 3f;
	
	void Start () {
		StartCoroutine("DisplaySence");
	}

	IEnumerator DisplaySence(){
		yield return new WaitForSeconds(timer);
		if(Attributes.userName != ""){
			Application.LoadLevel(Strings.SCEN_MENU);
		}else{
			Application.LoadLevel(Strings.SCEN_LOGIN);
		}
	}
}
