﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StarsController : MonoBehaviour {

	public Image[] stars; // list image of stars
	public Sprite spriteOn; //image stars on
	public Sprite spriteOff;// image stars off

	public void SetStars(int number)
	{
		// set sprite to list Image star
		for (int i = 0; i < stars.Length; i++)
		{
			if (i < number)
			{
				stars[i].sprite = spriteOn;
			}
			else
			{
				stars[i].sprite = spriteOff;
			}
		}
	}
}
