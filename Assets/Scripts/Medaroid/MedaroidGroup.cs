﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MedaroidGroup : MonoBehaviour
{
	public static MedaroidGroup instance;
	public GameObject medaroidGroup;
	public Transform container;
	public static Medaroid currentMedaroid;
	public Image selectedMedaroid; //Image selected medaroid 
	Medaroid[] medaroids;
	public Text txtName;
	public Text txtDetail;
	public StarsController size; //size star controller
	public StarsController weight; //weight star controller
	public StarsController speed; //speed star controller
	public static float posViewX = 0;
	public Sprite bgSelected;
	public MedaroidScrollItem current;
	public GameObject popupGetNew;// popup get new gacha
	
	void Start()
	{
		instance = this;
		
		medaroids = MedaroidLoader.LoadAll().ToArray();// Load all data from xml file
		for (int i = 0; i < 50; i++){
			GameObject o = Instantiate(medaroidGroup);
			o.transform.SetParent(container);
			o.transform.localScale = new Vector3(1, 1, 1);
			o.GetComponent<MedaroidGroupController>().SetSprites(medaroids[2 * i], medaroids[2 * i + 1]);
		}
		container.GetComponent<RectTransform>().anchoredPosition = new Vector2(3820, 0); 

		//Get current medaroid
		if (Attributes.GetMedaroid() != null)
		{
			SetSelectedMedaroid(Attributes.GetMedaroid());
		}
		//Get pos of view list medaroid
		if (MedaroidGroup.posViewX != 0)
		{
			container.GetComponent<RectTransform>().anchoredPosition = new Vector2(MedaroidGroup.posViewX, 0);
		}
	}
	// set index of medaroid selected to Image, Text, number Stars
	public void SetSelectedMedaroid(Medaroid selMed)
	{
		selectedMedaroid.sprite = selMed.sprite;
		txtName.text = selMed.name;
		txtDetail.text = selMed.description;
		size.SetStars(selMed.size);
		weight.SetStars(selMed.weight);
		speed.SetStars(selMed.speed);
		selectedMedaroid.GetComponent<Image>().SetNativeSize();
	}

	// Click on the locked item medaroid
	public void ClickUnlockGacha()
	{
		popupGetNew.SetActive(true);
	}
}
