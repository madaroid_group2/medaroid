﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class MedaroidScrollItem : MonoBehaviour
{
	
	public Sprite bgSelected;
	public Sprite bgDefault;
	
	public Sprite locked;
	
	public Image bgItemMedaroid;
	public Image imageItemMedaroid;
	public Medaroid medaroid;
	public GameObject popupGetNew;
	bool isClicked;

	public void OnClickMedaroidItem()
	{
		if (imageItemMedaroid.sprite != locked)
		{
			if (!isClicked)
			{
				SetActiveSelected(true);
				MedaroidGroup.instance.SetSelectedMedaroid(medaroid);
				MedaroidGroup.currentMedaroid = medaroid;
				isClicked = true;
			}
		}
		else
		{
			Instantiate(popupGetNew, popupGetNew.transform.position, Quaternion.identity);// create prefab gachaActive
			GameObject.Find("PopupGetNew(Clone)").transform.SetParent(GameObject.Find("Canvas").transform, false); //set position of gachanactive on canvas
		}
	}
	private void SetActiveSelected(bool isActive)
	{
		if (isActive)
		{
			this.GetComponent<Image>().sprite = bgSelected;
			if (MedaroidGroup.instance.current)
			{
				MedaroidGroup.instance.current.SetActiveSelected(false);
			}
			MedaroidGroup.instance.current = this;
			
		}
		else
		{
			this.GetComponent<Image>().sprite = bgDefault;
			isClicked = false;
		}
	}
	public void SetMedaroid(Medaroid med)
	{
		medaroid = med;
		if (med.isPurchased)
		{
			imageItemMedaroid.sprite = med.sprite;
		}
		else
		{
			imageItemMedaroid.sprite = locked;
		}
	}
}
