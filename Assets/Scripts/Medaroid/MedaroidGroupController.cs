﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MedaroidGroupController : MonoBehaviour
{
	
	public Medaroid leftMedaroid;
	public Medaroid rightMedaroid;
		
	public MedaroidScrollItem left;
	public MedaroidScrollItem right;
	
	bool isSelected;

	public void SetSprites(Medaroid medaroid1, Medaroid medaroid2)
	{
		left.SetMedaroid(medaroid1);
		right.SetMedaroid(medaroid2);
		leftMedaroid = medaroid1;
		rightMedaroid = medaroid2;
	}
}
