﻿/*
 * author: Hai Nguyen
 * date:10/1/2016
 * */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SettingContronller : MonoBehaviour {
	public Toggle toggleVideoRecord;
	public Toggle toggleNotification;
	public Toggle toggleBGM;
	public Toggle toggleSE;

	// Use this for initialization
	void Start () {
		if(Attributes.notification == 1){
			toggleNotification.isOn = true;
		}else if(Attributes.notification == 0){
			toggleNotification.isOn = false;
		}
		if (Attributes.videoRecord == 1) {
			toggleVideoRecord.isOn = true;
		} else if(Attributes.videoRecord == 0){
			toggleVideoRecord.isOn = false;
		}
		if (Attributes.bgm == 1) {
			toggleBGM.isOn = true;
		} else if (Attributes.bgm == 0){
			toggleBGM.isOn = false;
		}
		if (Attributes.se == 1) {
			toggleSE.isOn = true;
		} else if (Attributes.se == 0){
			toggleSE.isOn = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
	// change value setting Notification
	public void changeValueNotification(bool isOn){
		if (toggleNotification.isOn) {
			Attributes.SetNotification (1);
			Debug.Log("1");
		} else {
			Attributes.SetNotification(0);
			Debug.Log("0");
		}
	
	}
	// change value setting video record
	public void changeValueRecordVide(bool isOn){
		if (toggleVideoRecord.isOn) {
			Attributes.SetVideoRecorder (1);
			Debug.Log("1");
		} else {
			Attributes.SetVideoRecorder (0);
			Debug.Log("0");
		}
	}

	// change value setting BGM
	public void changeValueBGM(bool isOn){
		if (toggleBGM.isOn) {
			Attributes.SetBGM (1);
			Debug.Log("1");
		} else {
			Attributes.SetBGM(0);
			Debug.Log("0");
		}
	}

	// change value setting SE
	public void changeValueSE(bool isOn){
		if (toggleSE.isOn) {
			Attributes.SetSE (1);
			Debug.Log("1");
		} else {
			Attributes.SetSE(0);
			Debug.Log("0");
		}
	}
}
