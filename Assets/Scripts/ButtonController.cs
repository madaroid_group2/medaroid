﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class ButtonController : MonoBehaviour {
	public GameObject reviewPopup; // review popup get 10,000
	public GameObject recordPopup; // popup ghi hinh
	public GameObject poppupGacha; // hien thi popup get gacha
	public GameObject popupGachaFail; // hien thi popup get gacha fail
	public GameObject popupSetMedaroid; //popup set medaroid successful
	bool isMoveRight, isMoveLeft;
	public Transform container;// tranform of view list medaroid
	float timeCounter = 0; //time move slide view counter 
	//Back về Menu
	public void BackMenu(){
		Application.LoadLevel (Strings.SCEN_MENU);
	}

	// Update is called once per frame
	void Update()
	{
		//Smooth button left, right controller ---> list view medaroid
		if (isMoveRight && container.GetComponent<RectTransform>().anchoredPosition.x >= -3150 && !isMoveLeft)
		{
			container.GetComponent<RectTransform>().anchoredPosition -= new Vector2(15, 0);
			timeCounter += 1;
			if (timeCounter >= 40)
			{
				timeCounter = 0;
				isMoveRight = false;
			}
			
		}
		if (isMoveLeft && container.GetComponent<RectTransform>().anchoredPosition.x <= 3790 && !isMoveRight)
		{
			container.GetComponent<RectTransform>().anchoredPosition += new Vector2(15, 0);
			timeCounter += 1;
			if (timeCounter >= 40)
			{
				timeCounter = 0;
				isMoveLeft = false;
			}

		}
	}

	//-----------------MAIN MENU--------------------
	//go setting
	public void GoSetting(){
		Application.LoadLevel (Strings.SCEN_SETTING);
	}
	//go shop
	public void GoShop(){
		Application.LoadLevel(Strings.SCEN_SHOP);
	}
	// go play game
	public void GoGamePlay(){
		recordPopup.SetActive(true); //Khi click Play sẽ hiển thị Popup ghi hình
		//Application.LoadLevel(Strings.SCEN_GAMEPLAY);
	}
	//go gacha
	public void GoGacha(){
		Application.LoadLevel (Strings.SCEN_GACHA);
	}
	//go medaroid
	public void GoMedaroid(){
		Application.LoadLevel (Strings.SCEN_MEDAROID);
	}
	//go review popup get 10000
	public void ReviewPopupGetMoney(){
		reviewPopup.SetActive(true);
	}
	// popup ghi hinh, goto GamePlay Scene
	public void RecordPopup(){
		Application.LoadLevel(Strings.SCEN_GAMEPLAY);
		//recordPopup.SetActive(true);
	}

	// Set new gacha
	public void GachaActive(){
		poppupGacha.SetActive(true);
	}
	// Xac nhan YES active gacha
	public void YesActiveGacha(){
		// Kiem tra tien co du khong?
		if(Attributes.GetMoney() < Attributes.paidMoneyGacha){
			poppupGacha.SetActive(false);
			popupGachaFail.SetActive(true);
		}else{
			Attributes.SetMoney(Attributes.GetMoney() - Attributes.paidMoneyGacha);
			Application.LoadLevel(Strings.SCEN_GACHAEFFECT);
		}
	}
	// Xac nhan NO active gacha
	public void NoActiveGacha(){
		poppupGacha.SetActive(false);
	}

	// Xac nhan yes goto shop trong get gacha
	public void YesGoShop(){
		Application.LoadLevel(Strings.SCEN_SHOP);
	}

	// Xac nhan no goto shop trong get gacha
	public void NoGoShop(){
		popupGachaFail.SetActive(false);
	}

	//Click Yes to go Gacha Menu
	public void YesGetNew()
	{
		Destroy(GameObject.Find("PopupGetNew(Clone)"));
		Application.LoadLevel(Strings.SCEN_GACHA);
		Attributes.isClickYesGacha = true;
	}
	//Click No get new gacha
	public void NoGetNew()
	{
		Destroy(GameObject.Find("PopupGetNew(Clone)"));
	}
	//Click on the Right select medaroid button
	public void RightMedaroid()
	{
		isMoveRight = true;
		isMoveLeft = false;
		
	}
	//Click on the Left select medaroid button
	public void LeftMedaroid()
	{
		isMoveLeft = true;
		isMoveRight = false;
	}
	//Click on set medaroid button
	public void SetMedaroid()
	{
		popupSetMedaroid.SetActive(true);
	}
	//Click Ok set medaroid
	public void OKSetMedaroid()
	{
		popupSetMedaroid.SetActive(false);
		Attributes.SetMedaroid(MedaroidGroup.currentMedaroid);
		MedaroidGroup.posViewX = container.GetComponent<RectTransform>().anchoredPosition.x;
	}
}
