﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
public class XMLParsing : MonoBehaviour
{
	public TextMesh fileDataTextbox;
	private string path;
	private string fileInfo;
	private XmlDocument xmlDoc;
	private TextAsset textXml;
	private List<Medaroid> Medaroids;
	private string fileName;
	// Structure for mainitaing the Medaroid information
	struct Medaroid
	{
		public int id;
		public string origin;       
		public string name;         
		public string description;
		public int size;            
		public int weight;          
		public int speed;           
		public int power;           
		public int rarelity;        
		public float gachaPercent;  
		public int money;           
		public int score;     
	};
	void Awake()
	{
		fileName = "Data/Medaroid";
		Medaroids = new List<Medaroid>(); // initalize Medaroid list
		//fileDataTextbox.text = "";
	}
	void Start ()
	{
		loadXMLFromAssest();
		readXml();
	}
	// Following method load xml file from resouces folder under Assets
	private void loadXMLFromAssest()
	{
		xmlDoc = new XmlDocument();
		if(System.IO.File.Exists(getPath()))
		{
			xmlDoc.LoadXml(System.IO.File.ReadAllText(getPath()));
		}
		else
		{
			textXml = (TextAsset)Resources.Load(fileName, typeof(TextAsset));
			Debug.Log(textXml.text);
			xmlDoc.LoadXml(textXml.text);
		}
	}
	// Following method reads the xml file and display its content
	private void readXml()
	{
		foreach(XmlElement node in xmlDoc.SelectNodes("Medaroids/Medaroid"))
		{
			Medaroid tempMedaroid = new Medaroid();
			tempMedaroid.id = int.Parse(node.GetAttribute("id"));
			tempMedaroid.origin = node.SelectSingleNode("元ネタ").InnerText;
			tempMedaroid.name = node.SelectSingleNode("名前").InnerText;
			tempMedaroid.description = node.SelectSingleNode("説明文").InnerText;
			tempMedaroid.size = int.Parse(node.SelectSingleNode("大きさ").InnerText);
			tempMedaroid.weight = int.Parse(node.SelectSingleNode("重さ").InnerText);
			tempMedaroid.speed = int.Parse(node.SelectSingleNode("スピード").InnerText);
			tempMedaroid.power = int.Parse(node.SelectSingleNode("総合力").InnerText);
			tempMedaroid.rarelity = int.Parse(node.SelectSingleNode("レアリティ").InnerText);
			tempMedaroid.gachaPercent = float.Parse(node.SelectSingleNode("ガチャ出現率").InnerText);
			tempMedaroid.money = int.Parse(node.SelectSingleNode("所持マニー").InnerText);
			tempMedaroid.score = int.Parse(node.SelectSingleNode("所持スコア").InnerText);
			
			Medaroids.Add(tempMedaroid);
			displayMedaroidData(tempMedaroid);
		}
	}
	private void displayMedaroidData(Medaroid tempMedaroid)
	{
		fileDataTextbox.text += 	tempMedaroid.id + "\t\t" 
			+ tempMedaroid.origin + "\t\t" 
				+ tempMedaroid.name + "\t\t" 
				+ tempMedaroid.description + "\t\t" 
				+ tempMedaroid.size + "\t\t" 
				+ tempMedaroid.weight + "\t\t" 
				+ tempMedaroid.speed + "\t\t" 
				+ tempMedaroid.power + "\t\t" 
				+ tempMedaroid.rarelity + "\t\t" 
				+ tempMedaroid.gachaPercent + "\t\t" 
				+ tempMedaroid.money + "\t\t" 
				+ tempMedaroid.score + "\n";
	}
	// Following method is used to retrive the relative path as device platform
	private string getPath(){
		#if UNITY_EDITOR
		return Application.dataPath +"/Resources/Data"+fileName;
		#elif UNITY_ANDROID
		return Application.persistentDataPath+fileName;
		#elif UNITY_IPHONE
		return GetiPhoneDocumentsPath()+"/"+fileName;
		#else
		return Application.dataPath +"/"+ fileName;
		#endif
	}
	private string GetiPhoneDocumentsPath()
	{
		// Strip "/Data" from path
		string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
		// Strip application name
		path = path.Substring(0, path.LastIndexOf('/'));
		return path + "/Documents";
	}
}