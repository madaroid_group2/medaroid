﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/**
 * Author: Dao Danh Luu
 * 7/1/2016
 * Xu ly data cho man hinh GachanEffect
 * 
 */
public class GachanEffectController : MonoBehaviour {
	private float _time = 1.7f;
	private static bool isBackGacha = false; 
	public GameObject NewGachan;

	public Text txtName;
	public Text txtDetail;
	public Image imgMedaroid;
	public StarController sizeStars;
	public StarController weightStars;
	public StarController speedStars;
	public GameObject bgGacha;
	public GameObject textGacha;
	public Sprite gachaNew;
	public Sprite newText;

	Medaroid[] medaroids;
	// Use this for initialization
	void Start () {
		StartCoroutine (WaitNewGachan ());
		StartCoroutine (WaitBackGacharu ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	//Doi 1.7s den khi hien man hinh NewGachan
	IEnumerator WaitNewGachan(){
		yield return new WaitForSeconds(_time);
		NewGachan.SetActive (true);

	}
	//Đợi 5s để có thể Tap về màn hình Gacha
	IEnumerator WaitBackGacharu(){
		yield return new WaitForSeconds(5);
		isBackGacha = true;
		Debug.Log (isBackGacha);
	}
	//Back ve man hinh Gacha
	public void BackGacharu(){
		if (isBackGacha) {
			Debug.Log (isBackGacha);
		    Application.LoadLevel (Strings.SCEN_GACHA);
			isBackGacha = false;
			}
		}
	
}
